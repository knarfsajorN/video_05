import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
//import Checkbox from './checkbox';
import Button from './btn';

import check from '../images/ok.png';
import cancel from '../images/error.png';
import reset from '../images/refresh.png';

const Container = styled.div`
    display:flex;
    justify-content:space-evenly;
    align-items:center;
    flex-direction:row;
    margin-top : 15px;
    padding:15px 55px 15px 55px ;
    border-top: 1px solid #ffffff80 ;
`;

// const ContainerV2 = styled.div`
//     display:flex;
//     flex-direction:row;
//     justify-content:center;
//     align-items:center;
    
// `;

interface options {
    icon : string;
    onClick: () => void;
}

export interface params {
    enableReset ?: boolean;
    enableCancel ?: boolean;

    onReset ?: () => void ;
    onAccept ?: () => void ;
    onCancel ?: () => void ;
}

const App = (params:params):JSX.Element => {
    const handleReset = () => {
        if(typeof params.onReset === 'function') params.onReset();
    };

    const handleAcept = () => {
        if(typeof params.onAccept === 'function') params.onAccept();
    };

    const handleCancel = () => {
        if(typeof params.onCancel === 'function') params.onCancel();
    };

    const data : options[] = [
        {icon:check, onClick: handleAcept},
        //{icon:cancel, onClick:handleCancel},
        // {icon:reset, onClick: handleReset },
        // {icon:check, onClick: handleOk}
    ];

    if (params.enableReset) data.push({icon:reset, onClick: handleReset }) ;
    if (params.enableCancel) data.push({icon:cancel, onClick: handleCancel }); 

    return (
        <Container >
            {data.map((v,i) =><Button key={i} image={v.icon} onClick={v.onClick}/> )}
        </Container>
    );
  };

  App.defaultProps ={
    enableReset : true,
    enableCancel : true
  };
  
  export default App;